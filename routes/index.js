"use strict"

var express = require('express');
var basex = require('basex');
//var pd = require('pretty-data').pd;
var router = express.Router();
var _ = require('underscore');
var client = new basex.Session("localhost", 1984, "admin", "admin");
var namespace = "declare default element namespace 'http://www.tei-c.org/ns/1.0';";
var newFileLocation = "C:\\Users\\Eric Clay\\Documents\\VICTORIA\\SWEN303\\Assignment 1\\Assignment 12\\basex\\Colenso_TEIs";
client.execute("OPEN Colenso", function (err, data) {
    if (err) {
        console.log(err);
    } else {
        console.log(data);
    }
});

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Finder', user: 'Scott.'});
});

router.get('/testpage/', function (req, res, next) {

    res.render('testPage', {title: 'Express', user: 'Scott.'});
});

router.get('/jquery/', function (req, res, next) {
    res.render('jquery', {title: 'Express', user: 'Scott.'});
});

router.get('/jqueryResults/', function (req, res, next) {
    //console.log("Displaying results");
    var searchTerm = namespace + ' ' + req.query.term;
    var query = client.query(searchTerm);
    query.results(function (err, data) {
        if (!err) {
            //console.log(data.result);
            res.render('jqueryResults', {title: 'Express', user: 'Scott.', input: searchTerm, searchTerm: data.result});
        } else {
            res.render('ERR', {errMessage: err});
            console.log(err);
        }
    });
});

router.get('/searchPage/', function (req, res, next) {
    res.render('searchPage', {title: 'Express', user: 'Scott.'});
});

router.get('/edit/', function (req, res, next) {
    res.render('edit', {title: 'Express', user: 'Scott.'});
});

router.get('/addResults', function (req, res, next) {
    var status = "XML document has been added!";
    var xmldoc = req.query.xmldoc;
    var filepath = req.query.filepath;
    var directory = filepath;

    client.add(directory, xmldoc, function (err, data) {
        if (err) {
            console.log(err);
            status = ("XML upload to " + directory + " failed! \n" +
            "error: " + err);
        }
        if (!err) {
            status = "XML document has been added! \n" +
                "Added to " + filepath + "\n\n\n" +
                xmldoc;
        }
        res.render('addResults', {status: status});
    })


});

router.get('/resultsPage/', function (req, res, next) {
    client.execute("OPEN Colenso");
    //console.log("Displaying results");
    var searchTerm = req.query.term;
    var toQuery = "";


    if (searchTerm.indexOf(" ") != -1) {
        var stringBuild = searchTerm.split(" ");


        for (var i = 0; i < stringBuild.length; i++) {
            if (stringBuild[i] == "OR" || stringBuild[i] == "AND" || stringBuild[i] == "NOT") {
                if (stringBuild[i] == "OR") {
                    toQuery += 'union $doc/descendant-or-self::*[text() contains text "' + stringBuild[i + 1] + '"] ';
                    i++;
                } else if (stringBuild[i] == "AND") {
                    toQuery += 'intersect $doc/descendant-or-self::*[text() contains text "' + stringBuild[i + 1] + '"] ';
                    i++;
                } else if (stringBuild[i] == "NOT") {
                    toQuery += 'except $doc/descendant-or-self::*[text() contains text "' + stringBuild[i + 1] + '"] ';
                    i++;
                }

            } else {
                toQuery += '$doc/descendant-or-self::*[text() contains text "' + stringBuild[i] + '"] ';
            }
        }
    } else {
        toQuery += '$doc/descendant-or-self::*[text() contains text "' + searchTerm + '"] ';
    }

    //query = client.query(toQuery);
    var query = client.query("for $doc in collection('Colenso') where " + toQuery + " return $doc");
    //console.log("Query Complete with:  " + query);
    query.results(function (err, data) {
        if (err) {
            res.render('ERR', {errMessage: err});
            console.log(err);
        } else {
            var newarray = data.result;
            for (var i = 0; i < data.result.length; i++) {
                if (newarray[i].indexOf('<title>') > -1) {
                    newarray[i] = newarray[i].replace('<title>', '<newTitle>');
                }
                if (newarray[i].indexOf('</title>') > -1) {
                    newarray[i] = newarray[i].replace('</title>', '</newTitle>');
                }
                if (newarray[i].indexOf('<title xmlns="http://www.tei-c.org/ns/1.0">') > -1) {
                    newarray[i] = newarray[i].replace('<title xmlns="http://www.tei-c.org/ns/1.0">',
                        '<newTitle xmlns="http://www.tei-c.org/ns/1.0">');
                }
            }
            res.render('resultsPage', {title: 'Express', user: toQuery, searchTerm: newarray, size: newarray.length});
        }
    });
});

router.get('/browse/', function (req, res, next) {

    var roots = [];

    client.execute("LIST Colenso", function (err, data) {
        if (!err) {
            var results = data.result.split('\r\n'); //this is just one long string so must be split
            var duplicateList = [];


            for (var i = 2; i < results.length - 2; i++) {
                var file = results[i].split("/");
                duplicateList.push(file[0]);
            }

            var uniqueNames = [];
            var uniqueObj = [];

            for (var i = 0; i < duplicateList.length; i++) {
                if (uniqueNames.indexOf(duplicateList[i]) == -1) {
                    uniqueNames.push(duplicateList[i]);
                    var fileObj = {name: duplicateList[i], path: "/browse/folder/" + duplicateList[i]};
                    uniqueObj.push(fileObj);
                }
            }

            //console.log("RESULTS  " + uniqueNames);
            res.render('browse', {results: uniqueObj});

        } else {
            res.render('ERR', {errMessage: err});
        }
    });


    for (var i = 0; i < 12; i++) {
        roots[i] = 'Directory Name';
    }

    //  res.render('browse', {roots: roots});
});

router.get('/browse/folder/*', function (req, res, next) {
    var folder = req.originalUrl.replace("/browse/folder/", "");
    var toQuery = "for $doc in collection('Colenso') where matches(document-uri($doc), '/" + folder + "/') return db:path($doc)";
    var query = client.query(toQuery);
    query.results(function (err, data) {
        if(err){
            res.render('ERR', {errMessage: err});
        }else {
            var results = data.result;
            var objs = [];
            for (var i = 0; i < results.length; i++) {
                var path = results[i];
                path = path.replace(folder + "/", "");
                var split = path.split("/");
                var endPath = split[0];
                if (!_.contains(objs, endPath)) {
                    objs.push(endPath);
                }
            }

            var paths = [];

            for (var j = 0; j < objs.length; j++) {
                var object = objs[j];
                var pathToPush;
                if (object.indexOf(".xml") > -1) {
                    pathToPush = {path: object, url: "/browse/file/" + folder + "/" + object};

                } else {
                    pathToPush = {path: object, url: "/browse/folder/" + folder + "/" + object};

                }
                paths.push(pathToPush);
            }
            res.render('browse-folder', {directory: paths});
        }
    });
});

router.get('/browse/file/*', function (req, res, next) {
    var filename = req.originalUrl.replace("/browse/file/", "");
    var file = filename.split("/");
    //filename = file[file.length];


    var query = client.query("for $doc in collection('Colenso') where matches(document-uri($doc), '" + filename + "') return $doc");


    query.results(function (err, data) {
        if(err){
            res.render('ERR', {errMessage: err});
        }else {
            var results = data.result[0];


            if (results.indexOf('<title xmlns="http://www.tei-c.org/ns/1.0">') > -1) {
                results = results.replace('<title xmlns="http://www.tei-c.org/ns/1.0">',
                    '<newTitle xmlns="http://www.tei-c.org/ns/1.0">');
            }


            if (results.indexOf('<title>') > -1) {
                results = results.replace('<title>',
                    '<newTitle>');
            }


            if (results.indexOf('</title>') > -1) {
                results = results.replace('</title>',
                    '</newTitle>');
            }
            filename = file[file.length - 1];

            res.render('browse-file', {results: results, filename: filename});
        }
    });
});

router.get('/ERR/', function (req, res, next) {
    res.render('ERR', {errMessage: err});
});

module.exports = router;
